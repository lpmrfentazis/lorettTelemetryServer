from fastapi import FastAPI, Form, UploadFile, File
from datetime import datetime

from pathlib import Path


app = FastAPI()

@app.post("/upload")
def upload_file(
    station: str = Form(...),
    ftype: str = Form(...),
    file: UploadFile = File(...),
):
    out = Path().absolute() / "data" / file.filename
    
    # Сохраняем файл
    with open(out, "wb") as f:
        f.write(file.file.read())
    
    return {"station": station, "ftype": ftype, "file_saved": True}

if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8888)
